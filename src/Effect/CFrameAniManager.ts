class CFrameAniManager extends egret.DisplayObjectContainer {

      protected static s_containerEffects:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();



      public constructor() {
        super();

     

      } // constructor

      public static GetEffectsContainer():egret.DisplayObjectContainer  
        {
            return CFrameAniManager.s_containerEffects;
        }


      protected static s_lstRecycledEffects:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
      public static NewEffect():CFrameAni
      {
          var effect:CFrameAni = null;
          if ( CFrameAniManager.s_lstRecycledEffects.numChildren > 0 )
          {
              effect = CFrameAniManager.s_lstRecycledEffects.getChildAt(0) as CFrameAni;
              CFrameAniManager.s_lstRecycledEffects.removeChildAt(0);
          }
          else
          {
              effect = new CFrameAni();
          }

          CFrameAniManager.s_containerEffects.addChild( effect );

          return effect;
      }

      public static DeleteEffect( effect:CFrameAni ):void
      {
          CFrameAniManager.s_lstRecycledEffects.addChild( effect );
      }

      public static FixedUpdate():void
      {
          for ( var i:number = CFrameAniManager.s_containerEffects.numChildren - 1; i >= 0; i-- )
          {
              var effect:CFrameAni = CFrameAniManager.s_containerEffects.getChildAt(i) as CFrameAni;
              if ( effect.FixedUpdate() )
              {
                  // loop???
                  // to do

                  CFrameAniManager.DeleteEffect( effect );
              }
          } // end for`
      }

} // end class