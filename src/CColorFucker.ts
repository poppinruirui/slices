class CColorFucker extends egret.DisplayObjectContainer {


    // 根据传入的RGBA值，生成颜色滤镜
    // 白鹭引擎暂无内置的着色功能，只有用滤镜来实现着色效果
    protected static s_dicColorMatrixFilterByRGBA:Object = new Object();

    public static Init():void
    {
        CColorFucker.s_BlurDisabledGray = CColorFucker.GetColorMatrixFilterByRGBA_255( 190, 190, 190);
    }

    public static SetImageColor( img:eui.Image, r:number, g:number, b:number ):void
    {
        img.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( r, g, b ) ];
    }
    
    public static GetColorMatrixFilterByRGBA_255( r:number, g:number, b:number ):egret.ColorMatrixFilter
    {
        r /= 255;
        g /= 255;
        b /= 255;
        return CColorFucker.GetColorMatrixFilterByRGBA( r, g, b );
    }

    public static GetColorMatrixFilterByRGBA( r:number, g:number, b:number, a:number = 1):egret.ColorMatrixFilter
    {
        var szKey:string = r + "," + g + "," + "b" + "," + a;
        var colorFlilter:egret.ColorMatrixFilter = CColorFucker.s_dicColorMatrixFilterByRGBA[szKey];
        if ( colorFlilter == undefined )
        {
                    var colorMatrix = [
                    r,0,0,0,0,
                    0,g,0,0,0,
                    0,0,b,0,0,
                    a,a,a,0,0
                ];
              colorFlilter = new egret.ColorMatrixFilter(colorMatrix);
              CColorFucker.s_dicColorMatrixFilterByRGBA[szKey] = colorFlilter;
             
        }
        else
        {
         
        }
        
       
        return colorFlilter;
    }
 
    protected static   s_BlurFilter:egret.Filter = new egret.BlurFilter( 20 , 20);
    public static GetBlurFilter():egret.Filter{
            return CColorFucker.s_BlurFilter;
    }
    
    protected static   s_BlurDisabledGray:egret.Filter = null;
    public static GetDisabledGray():egret.Filter
    {
        return CColorFucker.s_BlurDisabledGray;
    }
      
        

    



} // end class
