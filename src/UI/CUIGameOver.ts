class CUIGameOver extends egret.DisplayObjectContainer {

     protected m_uiContainer:eui.Component = new eui.Component();

      public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/UIGameOver.exml";
        this.addChild( this.m_uiContainer );

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img0" ) as eui.Image;
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255( 0, 0, 0 )];

        this.touchEnabled = true;
        this.addEventListener( egret.TouchEvent.TOUCH_TAP, this.handleTapGameOverPanel, this );

   } // constructor

   protected handleTapGameOverPanel( evt:egret.TouchEvent ):void
   {
       CUIManager.SetUiVisible( Global.eUiId.game_over, false );
       Main.m_MainLogic.RestartGame();
   }

} // end class