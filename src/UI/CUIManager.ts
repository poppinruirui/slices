module Global{

   
    export enum eUiId{
        game_over,
        welcome,
        main,
    }
}


class CUIManager extends egret.DisplayObjectContainer {

 
     public static s_dicUIs:Object = new Object();

     public static s_containerUIs:egret.DisplayObjectContainer;
     
     public static s_uiGameOver:CUIGameOver; 
     public static s_uiWelcome:CUIWelcome; 
     public static s_uiMain:CUIMain; 

     public constructor() {
        super();

    
     } // end constructor


    public static Init():void
     {   
        CUIManager.s_uiGameOver = new CUIGameOver();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiGameOver );
        CUIManager.s_uiGameOver.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.game_over] = CUIManager.s_uiGameOver;

        CUIManager.s_uiWelcome = new CUIWelcome();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiWelcome );
        CUIManager.s_uiWelcome.visible = true;
        CUIManager.s_dicUIs[Global.eUiId.welcome] = CUIManager.s_uiWelcome;

        CUIManager.s_uiMain = new CUIMain();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiMain );
        CUIManager.s_uiMain.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.main] = CUIManager.s_uiMain;

     }

     public static SetUiVisible( eId:Global.eUiId, bVisible:boolean ):void
      {
          CUIManager.s_dicUIs[eId].visible = bVisible;
      }

} // end class