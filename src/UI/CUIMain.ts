class CUIMain extends egret.DisplayObjectContainer {

     protected m_uiContainer:eui.Component = new eui.Component();

     protected m_txtScore:eui.Label;

       public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/UIMain.exml";
        this.addChild( this.m_uiContainer );

        this.m_txtScore = this.m_uiContainer.getChildByName( "txt0" ) as eui.Label;

       } // end constructor

       public SetScore( nScore:number ):void
       {
           this.m_txtScore.text = nScore.toString();

       }

} // end class