class CMainLogic extends egret.DisplayObjectContainer {

    public static s_Instance:CMainLogic = null;

     protected MOVE_SPEED:number = 10;

     protected m_nCenterPlateX:number = 0;
     protected m_nCenterPlateY:number = 0;


     protected m_containerPlates:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
     protected m_dicPlates:Object = new Object();
    
     protected m_CenterPlate:CPlate = null;

     protected m_nPlateRadius:number = 80;

     protected m_bCan:boolean = true;
     protected m_vecMoveSpeed:CVector = new CVector();
     protected m_vecDestPos:CVector = new CVector();
     protected m_DestPlate:CPlate = null;
     protected m_CurArc:CArc = null;

     protected m_nMovingStatus:number = 0; // 0 - none  1 - move to dest  2 - move back

     protected m_nScore:number = 0;

     public constructor() {
        super();

        this.addChild( this.m_containerPlates );
       
        
    
     } // end constructor

     public GetPlateById( nId:number ):CPlate
     {
         return ( this.m_dicPlates[nId] as CPlate );
     }

     public Init( centerPlateX:number, centerPlateY:number ):void
     {
         this.m_nCenterPlateX = centerPlateX;
         this.m_nCenterPlateY = centerPlateY;

         var plate_radius:number = this.m_nPlateRadius;
         var plate_dis:number = plate_radius * 2.1;

         for ( var i:number = 0; i < 7; i++ )
         {
             var plate:CPlate = new CPlate();
             this.m_containerPlates.addChild( plate );

             if ( i == 0 )
             {
                 plate.x = this.m_nCenterPlateX;
                 plate.y = this.m_nCenterPlateY;
                 plate.Draw(  plate_radius,  10, false, 0x104E8B, 0x1C1C1C );
                 this.m_CenterPlate = plate;
             }  
             else
             {
                 var angle:number = CCyberTreeMath.Angle2Radian( -90 + i * 60 );
                 plate.x = this.m_nCenterPlateX + Math.cos( angle ) * plate_dis;
                 plate.y = this.m_nCenterPlateY + Math.sin( angle ) * plate_dis;
                 plate.Draw(  plate_radius,  4, true, 0x104E8B, 0x1C1C1C, 4 );
                 plate.touchEnabled = true;
                 plate.addEventListener( egret.TouchEvent.TOUCH_TAP, this.handleTapPlate, this )
             }

             plate.SetPlateId( i );
             this.m_dicPlates[i] = plate;
      
    
   }  // end for       

     }

     protected static m_contianerRecycledArcs:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();

     public static NewArc():CArc
     {

        var arc:CArc = null;
        if ( CMainLogic.m_contianerRecycledArcs.numChildren > 0 )
        {
            arc = CMainLogic.m_contianerRecycledArcs.getChildAt(0) as CArc;
            CMainLogic.m_contianerRecycledArcs.removeChildAt(0);
        } 
        else
        {
            arc = new CArc();
           
        }

        return arc;
     }

     public static DeleteArc( arc:CArc ):void
     {
         CMainLogic.m_contianerRecycledArcs.addChild( arc );
     }
     
     public GenerateArc():CArc
     {
         var arc:CArc = CMainLogic.NewArc();
         var fPercent:number = Math.random();
         var fSubPercent:number = Math.random();
         var nType:number = 0;
         var nPosIndex:number = Math.floor( 6 * Math.random() );
         if ( fPercent < 0.85 ) // 1型
         {
            nType = 1;
         }else if ( fPercent < 0.95 )// 2型
         {
            nType = 2;
         }
         else// 3型
         {
            nType = 3;
         }


         arc.SetParam( nType, nPosIndex, 0xFF4500, this.m_nPlateRadius );
         this.m_CenterPlate.SetArc( arc );

         // coming soon
         // to do
    
         if ( this.CheckIfGameOver(arc) )
         {
             CSoundManager.PlaySE( Global.eSE.game_over );
            CUIManager.SetUiVisible( Global.eUiId.game_over, true );
         }
      
         
         // to do

         return arc;
     }

     public CheckIfGameOver( arc:CArc ):boolean
     {

         for ( var i:number = 1; i < 7; i++ )
         {
             var plate:CPlate = this.m_containerPlates.getChildAt(i) as CPlate;
             if ( plate.CheckIfCanAdd( arc ) )
             {
                return false;
             }
         }   


         return true;
     }

     protected handleTapPlate( evt:egret.TouchEvent ):void
     {
         console.log( "000000" );
        var plate:CPlate = evt.currentTarget as CPlate;
        var arc:CArc = this.m_CenterPlate.GetArc() ;
        if (  arc == null )
        {
            return;
        }

        this.m_CenterPlate.SetArc( null ) ;

        this.m_bCan =  plate.CheckIfCanAdd( arc );
       
        this.m_CurArc = arc;
        this.m_DestPlate = plate;
  
        this.addChild(this.m_CurArc);
        this.m_CurArc.x = this.m_CenterPlate.x;
        this.m_CurArc.y = this.m_CenterPlate.y;
        
        this.BeginMove( 1 );

        //this.GenerateArc();
     }

     public FixedUpdate():void
     {
         this.MovingLoop();
     }

     public BeginMove( nStatus:number ):void
     {
          
         this.m_nMovingStatus = nStatus;
         if ( this.m_nMovingStatus == 1 )
         {   
            this.m_vecMoveSpeed.x = ( this.m_DestPlate.x - this.m_CenterPlate.x ) * this.MOVE_SPEED * Main.FIXED_DELTA_TIME;
            this.m_vecMoveSpeed.y = ( this.m_DestPlate.y - this.m_CenterPlate.y ) * this.MOVE_SPEED * Main.FIXED_DELTA_TIME;
            this.m_vecDestPos.x = this.m_DestPlate.x;
            this.m_vecDestPos.y = this.m_DestPlate.y;
         }
         else  if ( this.m_nMovingStatus == 2 )
         {    
            this.m_vecMoveSpeed.x = ( this.m_CenterPlate.x - this.m_DestPlate.x ) * this.MOVE_SPEED * Main.FIXED_DELTA_TIME;
            this.m_vecMoveSpeed.y = ( this.m_CenterPlate.y - this.m_DestPlate.y ) * this.MOVE_SPEED * Main.FIXED_DELTA_TIME;
            this.m_vecDestPos.x = this.m_CenterPlate.x;
            this.m_vecDestPos.y = this.m_CenterPlate.y;
         }
     }

     MovingLoop():void
     {
         if ( this.m_nMovingStatus == 0 )
         {
             return;
         }


         var bCompletedX:boolean = false;
         var bCompletedY:boolean = false;

         this.m_CurArc.x += this.m_vecMoveSpeed.x;
         this.m_CurArc.y += this.m_vecMoveSpeed.y;
         if ( this.m_vecMoveSpeed.x > 0 )
         {
             if ( this.m_CurArc.x >= this.m_vecDestPos.x  )
             {
                 bCompletedX = true;
             }
         }
         else if ( this.m_vecMoveSpeed.x < 0 )
         {
             if ( this.m_CurArc.x <= this.m_vecDestPos.x  )
             {
                 bCompletedX = true;
             }
         }
         else
         {
            bCompletedX = true;
         }

         if ( this.m_vecMoveSpeed.y > 0 )
         {
             if ( this.m_CurArc.y >= this.m_vecDestPos.y  )
             {
                 bCompletedY = true;
             }
         }
         else if ( this.m_vecMoveSpeed.y < 0 )
         {
             if ( this.m_CurArc.y <= this.m_vecDestPos.y  )
             {
                 bCompletedY = true;
             }
         }
         else
         {
            bCompletedY = true;
         }

         if ( bCompletedX && bCompletedY )
         {
             this.EndMove();
         }



     }

     public EndMove_Back():void
     {
         this.m_CenterPlate.SetArc( this.m_CurArc );

         this.m_nMovingStatus = 0;
     }

     public EndMove_ToDest():void
     { 
         if ( this.m_bCan ) // 可以放置 
         {
           

             var gain_score:number = 0;
             gain_score += this.m_CurArc.GetScore();
            
             CSoundManager.PlaySE( Global.eSE.lay_down );
         
            this.m_DestPlate.AddArc( this.m_CurArc );
           gain_score += this.m_DestPlate.ProcessFull();
         

            this.GenerateArc();

            this.m_nMovingStatus = 0;

        this.SetScore( this.GetScore() + gain_score );

         }
         else //不能放置
         {
             CSoundManager.PlaySE( Global.eSE.can_not_lay_down );
             this.BeginMove(2);
         }
     }

     public EndMove():void
     {
         this.m_CurArc.x = this.m_vecDestPos.x;
         this.m_CurArc.y = this.m_vecDestPos.y;

         if ( this.m_nMovingStatus == 1 )
         {  
             this.EndMove_ToDest();
         }
         else if ( this.m_nMovingStatus == 2 )
         {
             this.EndMove_Back();
         }else
          {
              console.log( "nali!!!!!!!!" );
          }


         
     }

     public Begin():void
     {
          var arc:CArc =  this.GenerateArc();
        //  CSoundManager.PlaySE( Global.eSE.wind_turbine );
     }
      
    public RestartGame():void
    {
        this.m_DestPlate = null;
        this.m_CurArc = null;
        this.m_nMovingStatus = 0;

        for ( var i:number = 0 ; i < 7; i++ )
        {
            var plate:CPlate = this.m_containerPlates.getChildAt(i) as CPlate;
            plate.Clear();
        }
        CSoundManager.PlaySE( Global.eSE.game_start );  
        this.Begin();

        this.SetScore(0);
    }


    public SetScore( nScore:number ):void
    {
        this.m_nScore = nScore;
        CUIManager.s_uiMain.SetScore( this.m_nScore );
       // console.log( "cur total score = " + this.m_nScore );
    }

    public GetScore():number
    {
        return this.m_nScore;
    }

} // end class