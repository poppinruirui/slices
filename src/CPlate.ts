class CPlate extends egret.DisplayObjectContainer {

     protected m_Circle:egret.Shape = new egret.Shape();
     protected m_Line0:egret.Shape;
     protected m_Line1:egret.Shape;
     protected m_Line2:egret.Shape;

     protected m_containerArcs:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();

     protected m_CenterArc:CArc = null;

     protected m_aryPosTaken:boolean[] = [false, false, false, false, false, false ];

     protected m_aryDebugInfo:Array<egret.TextField> = new Array<egret.TextField>();
     
     protected m_nPlateId:number = 0;

     public SetPlateId( nId:number ):void
     {
         this.m_nPlateId = nId;
     }

     public GetPlateId():number
     {
         return this.m_nPlateId;
     }

     public constructor() {
        super();

     

       this.addChild( this.m_Circle );
        this.addChild( this.m_containerArcs );
/*
        for ( var i:number = 1; i < 7; i++ )
        {
            var txt:egret.TextField = new egret.TextField();
            this.m_aryDebugInfo.push( txt );
            var angle:number = -70 + ( i - 1 ) * 60;
            txt.x = 50 * CCyberTreeMath.Cos( angle );
            txt.y = 50 * CCyberTreeMath.Sin( angle );
            txt.text = "0";
            this.addChild( txt );
        }
        */
     } // end constructor

     public GetScore():number
     {
         var nScore:number = 0;
         for ( var i:number = 0; i < this.m_aryPosTaken.length; i++ )
         {
             if ( this.m_aryPosTaken[i] )
             {
                 nScore++;
             }
         } // end for

         return nScore;

     }

     public Clear():void
     {
         this.m_CenterArc = null;

         for ( var i:number = this.m_containerArcs.numChildren - 1; i >= 0; i-- ) // 要移除节点时，必须反向遍历
         {
             var arc:CArc = this.m_containerArcs.getChildAt(i) as CArc;
             CMainLogic.DeleteArc( arc );
         } // end for

       
        for ( var i:number = 0;  i < 6; i++ )
        {
            this.m_aryPosTaken[i] = false;
        }
     }

     public SetPosTaken_ByArc( arc:CArc, bTaken:boolean ):void
     {
        var nStartPos:number = arc.GetStartPos();
        var nSizeType:number = arc.GetSizeType();
        var nEndPos:number = nStartPos + nSizeType;
       
        for ( var i:number = nStartPos; i < nEndPos; i++ )
        {
            var nIndex:number = i;
            if ( nIndex >= 6 )
            {
                nIndex -= 6;
            }
            
            this.SetPosTaken( nIndex, bTaken );     
        } // end for


     }

     public SetPosTaken( nPosIndex:number, bTaken:boolean ):void
     {
         this.m_aryPosTaken[nPosIndex] = bTaken;
         /*
         console.log( "nPosIndex=" + nPosIndex );
         if (  this.m_aryPosTaken[nPosIndex]  )
         {
             this.m_aryDebugInfo[nPosIndex].text = "1";
         }
         else
         {
              this.m_aryDebugInfo[nPosIndex].text = "0";
         }
         */
     }

     public GetPosTaken( nPosIndex:number ):boolean
     {
         return this.m_aryPosTaken[nPosIndex];
     }

     public Draw( radius:number, outline_thickness:number, show_line:boolean, outline_color:number, inside_color:number, inside_thickness:number  = 0 ):void
     {        
        this.m_Circle.graphics.lineStyle( outline_thickness, outline_color );
        this.m_Circle.graphics.beginFill( inside_color, 1 );
        this.m_Circle.graphics.drawCircle( 0, 0, radius );
        this.m_Circle.graphics.endFill();

   


        if ( show_line )
        {
            this.m_Line0 = new egret.Shape();
            this.m_Line1 = new egret.Shape();
            this.m_Line2 = new egret.Shape();
            this.addChild( this.m_Line0 );
            this.addChild( this.m_Line1 );
            this.addChild( this.m_Line2 );
            this.m_Line0.graphics.lineStyle( inside_thickness, outline_color );    
            this.m_Line0.graphics.moveTo( 0, -radius );
            this.m_Line0.graphics.lineTo( 0, radius );
            this.m_Line0.graphics.endFill();

            var angle:number =  CCyberTreeMath.Angle2Radian( 30 );
            var fSin:number = Math.sin( angle );
            var fCos:number = Math.cos( angle );
            this.m_Line1.graphics.lineStyle( inside_thickness, outline_color );    
            this.m_Line1.graphics.moveTo( radius * fCos, -radius * fSin );
            this.m_Line1.graphics.lineTo( -radius * fCos, radius * fSin );
            this.m_Line1.graphics.endFill();

            this.m_Line2.graphics.lineStyle( inside_thickness, outline_color );    
            this.m_Line2.graphics.moveTo( -radius * fCos, -radius * fSin );
            this.m_Line2.graphics.lineTo( radius * fCos, radius * fSin );
            this.m_Line2.graphics.endFill();
        
        }

     }

     public CheckIfCanAdd( arc:CArc ):boolean
     {
         var nStartPos:number = arc.GetStartPos();
         var nSizeType:number = arc.GetSizeType();

         // 
         var bCan:boolean = true;
        var nEndPos:number = nStartPos + nSizeType;
       
         for ( var i:number = nStartPos; i < nEndPos; i++ )
         {
             var nIndex:number = i;
            if ( nIndex >= 6 )
            {
                nIndex -= 6;
            }
            
             if ( this.GetPosTaken( nIndex ) )
             {
                 return false;
             }
         }

         return true;
     }

     public BeginMove( bCan:boolean ):void
     {

     }

     public SetArc( arc:CArc ):void
     {
         this.m_CenterArc = arc;  
         if ( this.m_CenterArc != null )
         {
             this.m_containerArcs.addChild( arc );
         
             this.m_CenterArc.x = 0;
             this.m_CenterArc.y = 0;
         }

     }

     public GetArc():CArc
     {
         return this.m_CenterArc;
     }

     public AddArc( arc:CArc ):void
     {
         this.m_containerArcs.addChild( arc );
         arc.x = 0;
         arc.y = 0;

         this.SetPosTaken_ByArc( arc, true );
     }

     public CheckIfFull():boolean
     {
         for ( var i:number = 0; i < 6; i++  )
         {
             if ( !this.m_aryPosTaken[i] )
             {
                 return false;
             }
         }

         return true;
     }

     public ProcessFullTiaoZi( szContent:string ):void
     {

             var tiaozi:CTiaoZi = CTiaoZiManager.NewTiaoZi();
             tiaozi.SetText(szContent);
             tiaozi.x = this.x;
             tiaozi.y = this.y;
             tiaozi.BeginTiaoZi();
     }

     public PlayFullEffect():void
     {
 
       var effect:CFrameAni = CFrameAniManager.NewEffect();
       effect.SetParams( "1a_", 12, 0, false );
       effect.Play();
       effect.x = this.x;
       effect.y = this.y;

     }

     public ProcessFull():number
     {
            var gain_score:number = 0;

            if ( !this.CheckIfFull() )
            {
                return 0;
            }

            CSoundManager.PlaySE( Global.eSE.coins );
           

            this.PlayFullEffect();
          
            // 相邻的两个圆盘都清空
            var nNeightbour1:number = this.GetPlateId() - 1;
            var nNeightbour2:number = this.GetPlateId() + 1;
     
            if ( nNeightbour1 == 0 )
            {
                nNeightbour1 = 6;
            }
            if ( nNeightbour2 == 7 )
            {
                nNeightbour2 = 1;
            }
            var plate_neighbour_1:CPlate = Main.m_MainLogic.GetPlateById( nNeightbour1 );
            var plate_neighbour_2:CPlate = Main.m_MainLogic.GetPlateById( nNeightbour2 );
            var score_of_this_plate:number = this.GetScore();
            gain_score += score_of_this_plate;
            this.ProcessFullTiaoZi( "+" + score_of_this_plate );
            score_of_this_plate = plate_neighbour_1.GetScore();
            plate_neighbour_1.ProcessFullTiaoZi( "+" + score_of_this_plate );
            gain_score += score_of_this_plate
            score_of_this_plate = plate_neighbour_2.GetScore();
            plate_neighbour_2.ProcessFullTiaoZi( "+" + score_of_this_plate );
            gain_score += score_of_this_plate;

 this.Clear();
            plate_neighbour_1.Clear();
            plate_neighbour_2.Clear();
            plate_neighbour_1.PlayFullEffect();
            plate_neighbour_2.PlayFullEffect();


      

            return gain_score;
     }
} // end class