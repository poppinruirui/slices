class CCyberTreeMath
{
    public static Angle2Radian( angle:number ):number
    {
        return Math.PI * angle / 180;
    }

    public static Sin( angle:number ):number
    {
        return Math.sin( CCyberTreeMath.Angle2Radian(angle) );
    }


    public static Cos( angle:number ):number
    {
        return Math.cos( CCyberTreeMath.Angle2Radian(angle) );
    }

    // 已知位移和时间求初速度（末速度为零）
    public static GetV0( s:number, t:number ):number
    {
        return (2.0 * s / t);
    }

    // 已知位移和时间求加速度（末速度为零）
    public static GetA( s:number,  t:number):number
    {
        return -2.0 * s / (t * t);
    }


} // end class