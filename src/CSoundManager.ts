 /*
  音频系统
 */
module Global{
    export enum eSE{ //  音效
        coins,
        congratulations,
        lay_down,
        wind_turbine,
        game_over,
        can_not_lay_down,
        game_start,

        num,
    }

}// end module Global

 class CSoundManager extends egret.DisplayObjectContainer {
 

     protected static s_dicSE:Object = new Object();

      public constructor() {
        super();
      }

     public static Init():void
     {
           

        var sound:egret.Sound = new egret.Sound();
        sound.load("resource/assets/Audio/coins.mp3");
        CSoundManager.s_dicSE[Global.eSE.coins] = sound; 
       
        sound = new egret.Sound();
        sound.load("resource/assets/Audio/congratulations.mp3");
        CSoundManager.s_dicSE[Global.eSE.congratulations] = sound; 

        sound = new egret.Sound();
        sound.load("resource/assets/Audio/laydoen.mp3");
        CSoundManager.s_dicSE[Global.eSE.lay_down] = sound; 

        sound = new egret.Sound();
        sound.load("resource/assets/Audio/windturbine.mp3");
        CSoundManager.s_dicSE[Global.eSE.wind_turbine] = sound; 

         sound = new egret.Sound();
        sound.load("resource/assets/Audio/game_over.mp3");
        CSoundManager.s_dicSE[Global.eSE.game_over] = sound; 
        
     sound = new egret.Sound();
        sound.load("resource/assets/Audio/can_not_lay_down.wav");
        CSoundManager.s_dicSE[Global.eSE.can_not_lay_down] = sound; 

        sound = new egret.Sound();
        sound.load("resource/assets/Audio/game_start.mp3");
        CSoundManager.s_dicSE[Global.eSE.game_start] = sound; 
        
        
     }
 
     public static PlaySE( eId:Global.eSE ):void
     {
       ( CSoundManager.s_dicSE[eId] as egret.Sound ).play( 0, 1 );
     }

    
 } // end class