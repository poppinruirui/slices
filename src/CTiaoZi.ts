class CTiaoZi extends egret.DisplayObjectContainer {

    protected m_bmpIcon:egret.Bitmap;
    protected m_txtValue:egret.TextField;
    protected m_txtValue_outline:egret.TextField;

    protected m_fV0:number = 0;
    protected m_fA:number = 0;
    protected m_fAlphaChangeSpeed:number = 0;
    protected m_fTimeElapse:number = 0;

   

    public Reset():void
    {
        this.alpha = 1;
    }

    public constructor() {
        super();

       

        this.m_txtValue = new egret.TextField();
       
        this.m_txtValue.text = "21.99M";
        this.m_txtValue.x = 80;
        this.m_txtValue.y = 10;
        this.m_txtValue.size = 75;
        this.m_txtValue.bold = true;


        this.m_txtValue_outline = new egret.TextField();
        this.addChild( this.m_txtValue_outline );
        this.m_txtValue_outline.text = this.m_txtValue.text;
        this.m_txtValue_outline.x = this.m_txtValue.x + 1;
        this.m_txtValue_outline.y = this.m_txtValue.y - 1;
        this.m_txtValue_outline.size = this.m_txtValue.size;
        this.m_txtValue_outline.bold = true;
        this.m_txtValue_outline.textColor = 0x000000;
        this.addChild( this.m_txtValue );

    } // end constructor


    public FixedUpdate():void
    {
        this.m_fTimeElapse += Main.FIXED_DELTA_TIME;
        if ( this.m_fTimeElapse >= Main.TIAOZI_TOTAL_TIME )
        {
            CTiaoZiManager.DeleteTiaoZi( this );
            return;
        }

        this.y += this.m_fV0;
      
        this.m_fV0 += this.m_fA;
        if ( this.m_fTimeElapse >= Main.TIAOZI_TOTAL_TIME * 0.5 )
        {
            this.alpha += this.m_fAlphaChangeSpeed;
        }
    }

    public BeginTiaoZi(  ):void
    {
        this.m_fV0 = CTiaoZiManager.s_fPosChangeV0 * Main.FIXED_DELTA_TIME;
        this.m_fA = CTiaoZiManager.s_fPosChangeA * Main.FIXED_DELTA_TIME;
        this.m_fAlphaChangeSpeed = CTiaoZiManager.s_fAlphaChangeSpeed * Main.FIXED_DELTA_TIME;
        this.m_fTimeElapse = 0;

        this.FixedUpdate();

    

    }

    public SetTypeAndValue(  nValue:number ):void{
        this.m_txtValue.text = (Math.floor(nValue)).toString();
        this.m_txtValue_outline.text = this.m_txtValue.text;
      
       
    }

    public  SetText( szContent:string ):void
    {
        this.m_txtValue.text = szContent;
        this.m_txtValue_outline.text = this.m_txtValue.text;
    } 

} // end class