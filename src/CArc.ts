

class CArc extends egret.DisplayObjectContainer {

     protected m_Arc:egret.Shape = new egret.Shape();

     protected m_nPos:number = 0;
     protected m_nType:number = 0;

 

     public constructor() {
        super();

       
   this.addChild( this.m_Arc );
    
     } // end constructor

     public Draw( color:number, x:number, y:number, radius:number, startAngle:number, endAngle:number, ):void
     {
          this.m_Arc.graphics.clear();
         this.m_Arc.graphics.beginFill( color, 1 );
         this.m_Arc.graphics.moveTo( 0, 0 );
          this.m_Arc.graphics.lineTo( radius * CCyberTreeMath.Cos( startAngle ), radius *  CCyberTreeMath.Sin( startAngle ) );
           this.m_Arc.graphics.lineTo( radius * CCyberTreeMath.Cos( endAngle ), radius *  CCyberTreeMath.Sin( endAngle )  );
            this.m_Arc.graphics.lineTo( 0, 0 );

         this.m_Arc.graphics.drawArc( x, y, radius, CCyberTreeMath.Angle2Radian( startAngle ), CCyberTreeMath.Angle2Radian( endAngle ) , false );
         this.m_Arc.graphics.endFill();

         this.m_Arc.cacheAsBitmap = true;
     }

     public SetParam( nType:number, nPosIndex:number, color:number, radius:number ):void
     {
         this.m_nPos = nPosIndex;
         this.m_nType = nType;

         radius -= 4;
         var startAngle:number =  nPosIndex * 60 - 90;
         var endAngle:number =  startAngle + nType * 60;
        
       this.Draw(  color, 0, 0, radius, startAngle, endAngle );

     }

     public GetStartPos():number
     {
         return this.m_nPos;
     }

     public GetSizeType():number
     {
         return this.m_nType;
     }

     public GetScore():number
     {
         return this.m_nType;
     }

} // end class