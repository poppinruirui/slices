var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CArc = (function (_super) {
    __extends(CArc, _super);
    function CArc() {
        var _this = _super.call(this) || this;
        _this.m_Arc = new egret.Shape();
        _this.m_nPos = 0;
        _this.m_nType = 0;
        _this.addChild(_this.m_Arc);
        return _this;
    } // end constructor
    CArc.prototype.Draw = function (color, x, y, radius, startAngle, endAngle) {
        this.m_Arc.graphics.clear();
        this.m_Arc.graphics.beginFill(color, 1);
        this.m_Arc.graphics.moveTo(0, 0);
        this.m_Arc.graphics.lineTo(radius * CCyberTreeMath.Cos(startAngle), radius * CCyberTreeMath.Sin(startAngle));
        this.m_Arc.graphics.lineTo(radius * CCyberTreeMath.Cos(endAngle), radius * CCyberTreeMath.Sin(endAngle));
        this.m_Arc.graphics.lineTo(0, 0);
        this.m_Arc.graphics.drawArc(x, y, radius, CCyberTreeMath.Angle2Radian(startAngle), CCyberTreeMath.Angle2Radian(endAngle), false);
        this.m_Arc.graphics.endFill();
        this.m_Arc.cacheAsBitmap = true;
    };
    CArc.prototype.SetParam = function (nType, nPosIndex, color, radius) {
        this.m_nPos = nPosIndex;
        this.m_nType = nType;
        radius -= 4;
        var startAngle = nPosIndex * 60 - 90;
        var endAngle = startAngle + nType * 60;
        this.Draw(color, 0, 0, radius, startAngle, endAngle);
    };
    CArc.prototype.GetStartPos = function () {
        return this.m_nPos;
    };
    CArc.prototype.GetSizeType = function () {
        return this.m_nType;
    };
    CArc.prototype.GetScore = function () {
        return this.m_nType;
    };
    return CArc;
}(egret.DisplayObjectContainer)); // end class
__reflect(CArc.prototype, "CArc");
//# sourceMappingURL=CArc.js.map