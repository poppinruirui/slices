var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var CTiaoZiManager = (function () {
    function CTiaoZiManager() {
    }
    CTiaoZiManager.Init = function () {
        CTiaoZiManager.s_fAlphaChangeSpeed = -1 / (Main.TIAOZI_TOTAL_TIME * 0.5);
        CTiaoZiManager.s_fPosChangeA = CCyberTreeMath.GetA(Main.TIAOZI_TOTAL_DISTANCE, Main.TIAOZI_TOTAL_TIME);
        CTiaoZiManager.s_fPosChangeV0 = CCyberTreeMath.GetV0(Main.TIAOZI_TOTAL_DISTANCE, Main.TIAOZI_TOTAL_TIME);
        console.log("跳字参数：" + CTiaoZiManager.s_fPosChangeA + "_" + CTiaoZiManager.s_fPosChangeV0);
    };
    CTiaoZiManager.NewTiaoZi = function () {
        var tiaozi = null;
        if (CTiaoZiManager.s_lstRecycledTiaoZi.numChildren > 0) {
            tiaozi = CTiaoZiManager.s_lstRecycledTiaoZi.getChildAt(0);
            CTiaoZiManager.s_lstRecycledTiaoZi.removeChildAt(0);
            tiaozi.Reset();
        }
        else {
            tiaozi = new CTiaoZi();
            tiaozi.anchorOffsetX = tiaozi.width / 2;
            tiaozi.anchorOffsetY = tiaozi.height;
        }
        CTiaoZiManager.s_lstcontainerTiaoZi.addChild(tiaozi);
        return tiaozi;
    };
    CTiaoZiManager.DeleteTiaoZi = function (tiaozi) {
        CTiaoZiManager.s_lstRecycledTiaoZi.addChild(tiaozi);
    };
    CTiaoZiManager.FixedUpdate = function () {
        for (var i = CTiaoZiManager.s_lstcontainerTiaoZi.numChildren - 1; i >= 0; i--) {
            var tiaozi = CTiaoZiManager.s_lstcontainerTiaoZi.getChildAt(i);
            tiaozi.FixedUpdate();
        }
    };
    CTiaoZiManager.s_lstRecycledTiaoZi = new egret.DisplayObjectContainer();
    CTiaoZiManager.s_lstcontainerTiaoZi = new egret.DisplayObjectContainer();
    CTiaoZiManager.s_fAlphaChangeSpeed = 0;
    CTiaoZiManager.s_fPosChangeA = 0;
    CTiaoZiManager.s_fPosChangeV0 = 0;
    return CTiaoZiManager;
}()); // end class
__reflect(CTiaoZiManager.prototype, "CTiaoZiManager");
//# sourceMappingURL=CTiaoZiManager.js.map