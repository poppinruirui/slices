var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/*
 音频系统
*/
var Global;
(function (Global) {
    var eSE;
    (function (eSE) {
        eSE[eSE["coins"] = 0] = "coins";
        eSE[eSE["congratulations"] = 1] = "congratulations";
        eSE[eSE["lay_down"] = 2] = "lay_down";
        eSE[eSE["wind_turbine"] = 3] = "wind_turbine";
        eSE[eSE["game_over"] = 4] = "game_over";
        eSE[eSE["can_not_lay_down"] = 5] = "can_not_lay_down";
        eSE[eSE["game_start"] = 6] = "game_start";
        eSE[eSE["num"] = 7] = "num";
    })(eSE = Global.eSE || (Global.eSE = {}));
})(Global || (Global = {})); // end module Global
var CSoundManager = (function (_super) {
    __extends(CSoundManager, _super);
    function CSoundManager() {
        return _super.call(this) || this;
    }
    CSoundManager.Init = function () {
        var sound = new egret.Sound();
        sound.load("resource/assets/Audio/coins.mp3");
        CSoundManager.s_dicSE[Global.eSE.coins] = sound;
        sound = new egret.Sound();
        sound.load("resource/assets/Audio/congratulations.mp3");
        CSoundManager.s_dicSE[Global.eSE.congratulations] = sound;
        sound = new egret.Sound();
        sound.load("resource/assets/Audio/laydoen.mp3");
        CSoundManager.s_dicSE[Global.eSE.lay_down] = sound;
        sound = new egret.Sound();
        sound.load("resource/assets/Audio/windturbine.mp3");
        CSoundManager.s_dicSE[Global.eSE.wind_turbine] = sound;
        sound = new egret.Sound();
        sound.load("resource/assets/Audio/game_over.mp3");
        CSoundManager.s_dicSE[Global.eSE.game_over] = sound;
        sound = new egret.Sound();
        sound.load("resource/assets/Audio/can_not_lay_down.wav");
        CSoundManager.s_dicSE[Global.eSE.can_not_lay_down] = sound;
        sound = new egret.Sound();
        sound.load("resource/assets/Audio/game_start.mp3");
        CSoundManager.s_dicSE[Global.eSE.game_start] = sound;
    };
    CSoundManager.PlaySE = function (eId) {
        CSoundManager.s_dicSE[eId].play(0, 1);
    };
    CSoundManager.s_dicSE = new Object();
    return CSoundManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CSoundManager.prototype, "CSoundManager");
//# sourceMappingURL=CSoundManager.js.map