var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CTiaoZi = (function (_super) {
    __extends(CTiaoZi, _super);
    function CTiaoZi() {
        var _this = _super.call(this) || this;
        _this.m_fV0 = 0;
        _this.m_fA = 0;
        _this.m_fAlphaChangeSpeed = 0;
        _this.m_fTimeElapse = 0;
        _this.m_txtValue = new egret.TextField();
        _this.m_txtValue.text = "21.99M";
        _this.m_txtValue.x = 80;
        _this.m_txtValue.y = 10;
        _this.m_txtValue.size = 75;
        _this.m_txtValue.bold = true;
        _this.m_txtValue_outline = new egret.TextField();
        _this.addChild(_this.m_txtValue_outline);
        _this.m_txtValue_outline.text = _this.m_txtValue.text;
        _this.m_txtValue_outline.x = _this.m_txtValue.x + 1;
        _this.m_txtValue_outline.y = _this.m_txtValue.y - 1;
        _this.m_txtValue_outline.size = _this.m_txtValue.size;
        _this.m_txtValue_outline.bold = true;
        _this.m_txtValue_outline.textColor = 0x000000;
        _this.addChild(_this.m_txtValue);
        return _this;
    } // end constructor
    CTiaoZi.prototype.Reset = function () {
        this.alpha = 1;
    };
    CTiaoZi.prototype.FixedUpdate = function () {
        this.m_fTimeElapse += Main.FIXED_DELTA_TIME;
        if (this.m_fTimeElapse >= Main.TIAOZI_TOTAL_TIME) {
            CTiaoZiManager.DeleteTiaoZi(this);
            return;
        }
        this.y += this.m_fV0;
        this.m_fV0 += this.m_fA;
        if (this.m_fTimeElapse >= Main.TIAOZI_TOTAL_TIME * 0.5) {
            this.alpha += this.m_fAlphaChangeSpeed;
        }
    };
    CTiaoZi.prototype.BeginTiaoZi = function () {
        this.m_fV0 = CTiaoZiManager.s_fPosChangeV0 * Main.FIXED_DELTA_TIME;
        this.m_fA = CTiaoZiManager.s_fPosChangeA * Main.FIXED_DELTA_TIME;
        this.m_fAlphaChangeSpeed = CTiaoZiManager.s_fAlphaChangeSpeed * Main.FIXED_DELTA_TIME;
        this.m_fTimeElapse = 0;
        this.FixedUpdate();
    };
    CTiaoZi.prototype.SetTypeAndValue = function (nValue) {
        this.m_txtValue.text = (Math.floor(nValue)).toString();
        this.m_txtValue_outline.text = this.m_txtValue.text;
    };
    CTiaoZi.prototype.SetText = function (szContent) {
        this.m_txtValue.text = szContent;
        this.m_txtValue_outline.text = this.m_txtValue.text;
    };
    return CTiaoZi;
}(egret.DisplayObjectContainer)); // end class
__reflect(CTiaoZi.prototype, "CTiaoZi");
//# sourceMappingURL=CTiaoZi.js.map