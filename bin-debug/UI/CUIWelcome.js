var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIWelcome = (function (_super) {
    __extends(CUIWelcome, _super);
    function CUIWelcome() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_timerMainLoop1 = new egret.Timer(2000, 1);
        _this.m_timerMainLoop2 = new egret.Timer(60, 0);
        _this.m_uiContainer.skinName = "resource/assets/MyExml/UIWelcome.exml";
        _this.addChild(_this.m_uiContainer);
        var imgTemp = _this.m_uiContainer.getChildByName("img0");
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(255, 110, 180)];
        _this.m_timerMainLoop1.addEventListener(egret.TimerEvent.TIMER, _this.Status1, _this);
        _this.m_timerMainLoop1.start();
        _this.m_timerMainLoop2.addEventListener(egret.TimerEvent.TIMER, _this.Status2, _this);
        return _this;
    } // constructor
    CUIWelcome.prototype.Status1 = function () {
        this.m_timerMainLoop2.start();
    };
    CUIWelcome.prototype.Status2 = function () {
        this.alpha -= 0.06;
        if (this.alpha <= 0) {
            this.m_timerMainLoop2.stop();
            CUIManager.SetUiVisible(Global.eUiId.welcome, false);
            CSoundManager.PlaySE(Global.eSE.game_start);
            CUIManager.SetUiVisible(Global.eUiId.main, true);
        }
    };
    return CUIWelcome;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIWelcome.prototype, "CUIWelcome");
//# sourceMappingURL=CUIWelcome.js.map