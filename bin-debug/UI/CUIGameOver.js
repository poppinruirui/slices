var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIGameOver = (function (_super) {
    __extends(CUIGameOver, _super);
    function CUIGameOver() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/UIGameOver.exml";
        _this.addChild(_this.m_uiContainer);
        var imgTemp = _this.m_uiContainer.getChildByName("img0");
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(0, 0, 0)];
        _this.touchEnabled = true;
        _this.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.handleTapGameOverPanel, _this);
        return _this;
    } // constructor
    CUIGameOver.prototype.handleTapGameOverPanel = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.game_over, false);
        Main.m_MainLogic.RestartGame();
    };
    return CUIGameOver;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIGameOver.prototype, "CUIGameOver");
//# sourceMappingURL=CUIGameOver.js.map