var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var Global;
(function (Global) {
    var eUiId;
    (function (eUiId) {
        eUiId[eUiId["game_over"] = 0] = "game_over";
        eUiId[eUiId["welcome"] = 1] = "welcome";
        eUiId[eUiId["main"] = 2] = "main";
    })(eUiId = Global.eUiId || (Global.eUiId = {}));
})(Global || (Global = {}));
var CUIManager = (function (_super) {
    __extends(CUIManager, _super);
    function CUIManager() {
        return _super.call(this) || this;
    } // end constructor
    CUIManager.Init = function () {
        CUIManager.s_uiGameOver = new CUIGameOver();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiGameOver);
        CUIManager.s_uiGameOver.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.game_over] = CUIManager.s_uiGameOver;
        CUIManager.s_uiWelcome = new CUIWelcome();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiWelcome);
        CUIManager.s_uiWelcome.visible = true;
        CUIManager.s_dicUIs[Global.eUiId.welcome] = CUIManager.s_uiWelcome;
        CUIManager.s_uiMain = new CUIMain();
        CUIManager.s_containerUIs.addChild(CUIManager.s_uiMain);
        CUIManager.s_uiMain.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.main] = CUIManager.s_uiMain;
    };
    CUIManager.SetUiVisible = function (eId, bVisible) {
        CUIManager.s_dicUIs[eId].visible = bVisible;
    };
    CUIManager.s_dicUIs = new Object();
    return CUIManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIManager.prototype, "CUIManager");
//# sourceMappingURL=CUIManager.js.map