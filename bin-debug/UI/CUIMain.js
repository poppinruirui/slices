var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIMain = (function (_super) {
    __extends(CUIMain, _super);
    function CUIMain() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/UIMain.exml";
        _this.addChild(_this.m_uiContainer);
        _this.m_txtScore = _this.m_uiContainer.getChildByName("txt0");
        return _this;
    } // end constructor
    CUIMain.prototype.SetScore = function (nScore) {
        this.m_txtScore.text = nScore.toString();
    };
    return CUIMain;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIMain.prototype, "CUIMain");
//# sourceMappingURL=CUIMain.js.map