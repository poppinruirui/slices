var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CPlate = (function (_super) {
    __extends(CPlate, _super);
    function CPlate() {
        var _this = _super.call(this) || this;
        _this.m_Circle = new egret.Shape();
        _this.m_containerArcs = new egret.DisplayObjectContainer();
        _this.m_CenterArc = null;
        _this.m_aryPosTaken = [false, false, false, false, false, false];
        _this.m_aryDebugInfo = new Array();
        _this.m_nPlateId = 0;
        _this.addChild(_this.m_Circle);
        _this.addChild(_this.m_containerArcs);
        return _this;
        /*
                for ( var i:number = 1; i < 7; i++ )
                {
                    var txt:egret.TextField = new egret.TextField();
                    this.m_aryDebugInfo.push( txt );
                    var angle:number = -70 + ( i - 1 ) * 60;
                    txt.x = 50 * CCyberTreeMath.Cos( angle );
                    txt.y = 50 * CCyberTreeMath.Sin( angle );
                    txt.text = "0";
                    this.addChild( txt );
                }
                */
    } // end constructor
    CPlate.prototype.SetPlateId = function (nId) {
        this.m_nPlateId = nId;
    };
    CPlate.prototype.GetPlateId = function () {
        return this.m_nPlateId;
    };
    CPlate.prototype.GetScore = function () {
        var nScore = 0;
        for (var i = 0; i < this.m_aryPosTaken.length; i++) {
            if (this.m_aryPosTaken[i]) {
                nScore++;
            }
        } // end for
        return nScore;
    };
    CPlate.prototype.Clear = function () {
        this.m_CenterArc = null;
        for (var i = this.m_containerArcs.numChildren - 1; i >= 0; i--) {
            var arc = this.m_containerArcs.getChildAt(i);
            CMainLogic.DeleteArc(arc);
        } // end for
        for (var i = 0; i < 6; i++) {
            this.m_aryPosTaken[i] = false;
        }
    };
    CPlate.prototype.SetPosTaken_ByArc = function (arc, bTaken) {
        var nStartPos = arc.GetStartPos();
        var nSizeType = arc.GetSizeType();
        var nEndPos = nStartPos + nSizeType;
        for (var i = nStartPos; i < nEndPos; i++) {
            var nIndex = i;
            if (nIndex >= 6) {
                nIndex -= 6;
            }
            this.SetPosTaken(nIndex, bTaken);
        } // end for
    };
    CPlate.prototype.SetPosTaken = function (nPosIndex, bTaken) {
        this.m_aryPosTaken[nPosIndex] = bTaken;
        /*
        console.log( "nPosIndex=" + nPosIndex );
        if (  this.m_aryPosTaken[nPosIndex]  )
        {
            this.m_aryDebugInfo[nPosIndex].text = "1";
        }
        else
        {
             this.m_aryDebugInfo[nPosIndex].text = "0";
        }
        */
    };
    CPlate.prototype.GetPosTaken = function (nPosIndex) {
        return this.m_aryPosTaken[nPosIndex];
    };
    CPlate.prototype.Draw = function (radius, outline_thickness, show_line, outline_color, inside_color, inside_thickness) {
        if (inside_thickness === void 0) { inside_thickness = 0; }
        this.m_Circle.graphics.lineStyle(outline_thickness, outline_color);
        this.m_Circle.graphics.beginFill(inside_color, 1);
        this.m_Circle.graphics.drawCircle(0, 0, radius);
        this.m_Circle.graphics.endFill();
        if (show_line) {
            this.m_Line0 = new egret.Shape();
            this.m_Line1 = new egret.Shape();
            this.m_Line2 = new egret.Shape();
            this.addChild(this.m_Line0);
            this.addChild(this.m_Line1);
            this.addChild(this.m_Line2);
            this.m_Line0.graphics.lineStyle(inside_thickness, outline_color);
            this.m_Line0.graphics.moveTo(0, -radius);
            this.m_Line0.graphics.lineTo(0, radius);
            this.m_Line0.graphics.endFill();
            var angle = CCyberTreeMath.Angle2Radian(30);
            var fSin = Math.sin(angle);
            var fCos = Math.cos(angle);
            this.m_Line1.graphics.lineStyle(inside_thickness, outline_color);
            this.m_Line1.graphics.moveTo(radius * fCos, -radius * fSin);
            this.m_Line1.graphics.lineTo(-radius * fCos, radius * fSin);
            this.m_Line1.graphics.endFill();
            this.m_Line2.graphics.lineStyle(inside_thickness, outline_color);
            this.m_Line2.graphics.moveTo(-radius * fCos, -radius * fSin);
            this.m_Line2.graphics.lineTo(radius * fCos, radius * fSin);
            this.m_Line2.graphics.endFill();
        }
    };
    CPlate.prototype.CheckIfCanAdd = function (arc) {
        var nStartPos = arc.GetStartPos();
        var nSizeType = arc.GetSizeType();
        // 
        var bCan = true;
        var nEndPos = nStartPos + nSizeType;
        for (var i = nStartPos; i < nEndPos; i++) {
            var nIndex = i;
            if (nIndex >= 6) {
                nIndex -= 6;
            }
            if (this.GetPosTaken(nIndex)) {
                return false;
            }
        }
        return true;
    };
    CPlate.prototype.BeginMove = function (bCan) {
    };
    CPlate.prototype.SetArc = function (arc) {
        this.m_CenterArc = arc;
        if (this.m_CenterArc != null) {
            this.m_containerArcs.addChild(arc);
            this.m_CenterArc.x = 0;
            this.m_CenterArc.y = 0;
        }
    };
    CPlate.prototype.GetArc = function () {
        return this.m_CenterArc;
    };
    CPlate.prototype.AddArc = function (arc) {
        this.m_containerArcs.addChild(arc);
        arc.x = 0;
        arc.y = 0;
        this.SetPosTaken_ByArc(arc, true);
    };
    CPlate.prototype.CheckIfFull = function () {
        for (var i = 0; i < 6; i++) {
            if (!this.m_aryPosTaken[i]) {
                return false;
            }
        }
        return true;
    };
    CPlate.prototype.ProcessFullTiaoZi = function (szContent) {
        var tiaozi = CTiaoZiManager.NewTiaoZi();
        tiaozi.SetText(szContent);
        tiaozi.x = this.x;
        tiaozi.y = this.y;
        tiaozi.BeginTiaoZi();
    };
    CPlate.prototype.PlayFullEffect = function () {
        var effect = CFrameAniManager.NewEffect();
        effect.SetParams("1a_", 12, 0, false);
        effect.Play();
        effect.x = this.x;
        effect.y = this.y;
    };
    CPlate.prototype.ProcessFull = function () {
        var gain_score = 0;
        if (!this.CheckIfFull()) {
            return 0;
        }
        CSoundManager.PlaySE(Global.eSE.coins);
        this.PlayFullEffect();
        // 相邻的两个圆盘都清空
        var nNeightbour1 = this.GetPlateId() - 1;
        var nNeightbour2 = this.GetPlateId() + 1;
        if (nNeightbour1 == 0) {
            nNeightbour1 = 6;
        }
        if (nNeightbour2 == 7) {
            nNeightbour2 = 1;
        }
        var plate_neighbour_1 = Main.m_MainLogic.GetPlateById(nNeightbour1);
        var plate_neighbour_2 = Main.m_MainLogic.GetPlateById(nNeightbour2);
        var score_of_this_plate = this.GetScore();
        gain_score += score_of_this_plate;
        this.ProcessFullTiaoZi("+" + score_of_this_plate);
        score_of_this_plate = plate_neighbour_1.GetScore();
        plate_neighbour_1.ProcessFullTiaoZi("+" + score_of_this_plate);
        gain_score += score_of_this_plate;
        score_of_this_plate = plate_neighbour_2.GetScore();
        plate_neighbour_2.ProcessFullTiaoZi("+" + score_of_this_plate);
        gain_score += score_of_this_plate;
        this.Clear();
        plate_neighbour_1.Clear();
        plate_neighbour_2.Clear();
        plate_neighbour_1.PlayFullEffect();
        plate_neighbour_2.PlayFullEffect();
        return gain_score;
    };
    return CPlate;
}(egret.DisplayObjectContainer)); // end class
__reflect(CPlate.prototype, "CPlate");
//# sourceMappingURL=CPlate.js.map