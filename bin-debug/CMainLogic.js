var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CMainLogic = (function (_super) {
    __extends(CMainLogic, _super);
    function CMainLogic() {
        var _this = _super.call(this) || this;
        _this.MOVE_SPEED = 10;
        _this.m_nCenterPlateX = 0;
        _this.m_nCenterPlateY = 0;
        _this.m_containerPlates = new egret.DisplayObjectContainer();
        _this.m_dicPlates = new Object();
        _this.m_CenterPlate = null;
        _this.m_nPlateRadius = 80;
        _this.m_bCan = true;
        _this.m_vecMoveSpeed = new CVector();
        _this.m_vecDestPos = new CVector();
        _this.m_DestPlate = null;
        _this.m_CurArc = null;
        _this.m_nMovingStatus = 0; // 0 - none  1 - move to dest  2 - move back
        _this.m_nScore = 0;
        _this.addChild(_this.m_containerPlates);
        return _this;
    } // end constructor
    CMainLogic.prototype.GetPlateById = function (nId) {
        return this.m_dicPlates[nId];
    };
    CMainLogic.prototype.Init = function (centerPlateX, centerPlateY) {
        this.m_nCenterPlateX = centerPlateX;
        this.m_nCenterPlateY = centerPlateY;
        var plate_radius = this.m_nPlateRadius;
        var plate_dis = plate_radius * 2.1;
        for (var i = 0; i < 7; i++) {
            var plate = new CPlate();
            this.m_containerPlates.addChild(plate);
            if (i == 0) {
                plate.x = this.m_nCenterPlateX;
                plate.y = this.m_nCenterPlateY;
                plate.Draw(plate_radius, 10, false, 0x104E8B, 0x1C1C1C);
                this.m_CenterPlate = plate;
            }
            else {
                var angle = CCyberTreeMath.Angle2Radian(-90 + i * 60);
                plate.x = this.m_nCenterPlateX + Math.cos(angle) * plate_dis;
                plate.y = this.m_nCenterPlateY + Math.sin(angle) * plate_dis;
                plate.Draw(plate_radius, 4, true, 0x104E8B, 0x1C1C1C, 4);
                plate.touchEnabled = true;
                plate.addEventListener(egret.TouchEvent.TOUCH_TAP, this.handleTapPlate, this);
            }
            plate.SetPlateId(i);
            this.m_dicPlates[i] = plate;
        } // end for       
    };
    CMainLogic.NewArc = function () {
        var arc = null;
        if (CMainLogic.m_contianerRecycledArcs.numChildren > 0) {
            arc = CMainLogic.m_contianerRecycledArcs.getChildAt(0);
            CMainLogic.m_contianerRecycledArcs.removeChildAt(0);
        }
        else {
            arc = new CArc();
        }
        return arc;
    };
    CMainLogic.DeleteArc = function (arc) {
        CMainLogic.m_contianerRecycledArcs.addChild(arc);
    };
    CMainLogic.prototype.GenerateArc = function () {
        var arc = CMainLogic.NewArc();
        var fPercent = Math.random();
        var fSubPercent = Math.random();
        var nType = 0;
        var nPosIndex = Math.floor(6 * Math.random());
        if (fPercent < 0.85) {
            nType = 1;
        }
        else if (fPercent < 0.95) {
            nType = 2;
        }
        else {
            nType = 3;
        }
        arc.SetParam(nType, nPosIndex, 0xFF4500, this.m_nPlateRadius);
        this.m_CenterPlate.SetArc(arc);
        // coming soon
        // to do
        if (this.CheckIfGameOver(arc)) {
            CSoundManager.PlaySE(Global.eSE.game_over);
            CUIManager.SetUiVisible(Global.eUiId.game_over, true);
        }
        // to do
        return arc;
    };
    CMainLogic.prototype.CheckIfGameOver = function (arc) {
        for (var i = 1; i < 7; i++) {
            var plate = this.m_containerPlates.getChildAt(i);
            if (plate.CheckIfCanAdd(arc)) {
                return false;
            }
        }
        return true;
    };
    CMainLogic.prototype.handleTapPlate = function (evt) {
        console.log("000000");
        var plate = evt.currentTarget;
        var arc = this.m_CenterPlate.GetArc();
        if (arc == null) {
            return;
        }
        this.m_CenterPlate.SetArc(null);
        this.m_bCan = plate.CheckIfCanAdd(arc);
        this.m_CurArc = arc;
        this.m_DestPlate = plate;
        this.addChild(this.m_CurArc);
        this.m_CurArc.x = this.m_CenterPlate.x;
        this.m_CurArc.y = this.m_CenterPlate.y;
        this.BeginMove(1);
        //this.GenerateArc();
    };
    CMainLogic.prototype.FixedUpdate = function () {
        this.MovingLoop();
    };
    CMainLogic.prototype.BeginMove = function (nStatus) {
        this.m_nMovingStatus = nStatus;
        if (this.m_nMovingStatus == 1) {
            this.m_vecMoveSpeed.x = (this.m_DestPlate.x - this.m_CenterPlate.x) * this.MOVE_SPEED * Main.FIXED_DELTA_TIME;
            this.m_vecMoveSpeed.y = (this.m_DestPlate.y - this.m_CenterPlate.y) * this.MOVE_SPEED * Main.FIXED_DELTA_TIME;
            this.m_vecDestPos.x = this.m_DestPlate.x;
            this.m_vecDestPos.y = this.m_DestPlate.y;
        }
        else if (this.m_nMovingStatus == 2) {
            this.m_vecMoveSpeed.x = (this.m_CenterPlate.x - this.m_DestPlate.x) * this.MOVE_SPEED * Main.FIXED_DELTA_TIME;
            this.m_vecMoveSpeed.y = (this.m_CenterPlate.y - this.m_DestPlate.y) * this.MOVE_SPEED * Main.FIXED_DELTA_TIME;
            this.m_vecDestPos.x = this.m_CenterPlate.x;
            this.m_vecDestPos.y = this.m_CenterPlate.y;
        }
    };
    CMainLogic.prototype.MovingLoop = function () {
        if (this.m_nMovingStatus == 0) {
            return;
        }
        var bCompletedX = false;
        var bCompletedY = false;
        this.m_CurArc.x += this.m_vecMoveSpeed.x;
        this.m_CurArc.y += this.m_vecMoveSpeed.y;
        if (this.m_vecMoveSpeed.x > 0) {
            if (this.m_CurArc.x >= this.m_vecDestPos.x) {
                bCompletedX = true;
            }
        }
        else if (this.m_vecMoveSpeed.x < 0) {
            if (this.m_CurArc.x <= this.m_vecDestPos.x) {
                bCompletedX = true;
            }
        }
        else {
            bCompletedX = true;
        }
        if (this.m_vecMoveSpeed.y > 0) {
            if (this.m_CurArc.y >= this.m_vecDestPos.y) {
                bCompletedY = true;
            }
        }
        else if (this.m_vecMoveSpeed.y < 0) {
            if (this.m_CurArc.y <= this.m_vecDestPos.y) {
                bCompletedY = true;
            }
        }
        else {
            bCompletedY = true;
        }
        if (bCompletedX && bCompletedY) {
            this.EndMove();
        }
    };
    CMainLogic.prototype.EndMove_Back = function () {
        this.m_CenterPlate.SetArc(this.m_CurArc);
        this.m_nMovingStatus = 0;
    };
    CMainLogic.prototype.EndMove_ToDest = function () {
        if (this.m_bCan) {
            var gain_score = 0;
            gain_score += this.m_CurArc.GetScore();
            CSoundManager.PlaySE(Global.eSE.lay_down);
            this.m_DestPlate.AddArc(this.m_CurArc);
            gain_score += this.m_DestPlate.ProcessFull();
            this.GenerateArc();
            this.m_nMovingStatus = 0;
            this.SetScore(this.GetScore() + gain_score);
        }
        else {
            CSoundManager.PlaySE(Global.eSE.can_not_lay_down);
            this.BeginMove(2);
        }
    };
    CMainLogic.prototype.EndMove = function () {
        this.m_CurArc.x = this.m_vecDestPos.x;
        this.m_CurArc.y = this.m_vecDestPos.y;
        if (this.m_nMovingStatus == 1) {
            this.EndMove_ToDest();
        }
        else if (this.m_nMovingStatus == 2) {
            this.EndMove_Back();
        }
        else {
            console.log("nali!!!!!!!!");
        }
    };
    CMainLogic.prototype.Begin = function () {
        var arc = this.GenerateArc();
        //  CSoundManager.PlaySE( Global.eSE.wind_turbine );
    };
    CMainLogic.prototype.RestartGame = function () {
        this.m_DestPlate = null;
        this.m_CurArc = null;
        this.m_nMovingStatus = 0;
        for (var i = 0; i < 7; i++) {
            var plate = this.m_containerPlates.getChildAt(i);
            plate.Clear();
        }
        CSoundManager.PlaySE(Global.eSE.game_start);
        this.Begin();
        this.SetScore(0);
    };
    CMainLogic.prototype.SetScore = function (nScore) {
        this.m_nScore = nScore;
        CUIManager.s_uiMain.SetScore(this.m_nScore);
        // console.log( "cur total score = " + this.m_nScore );
    };
    CMainLogic.prototype.GetScore = function () {
        return this.m_nScore;
    };
    CMainLogic.s_Instance = null;
    CMainLogic.m_contianerRecycledArcs = new egret.DisplayObjectContainer();
    return CMainLogic;
}(egret.DisplayObjectContainer)); // end class
__reflect(CMainLogic.prototype, "CMainLogic");
//# sourceMappingURL=CMainLogic.js.map