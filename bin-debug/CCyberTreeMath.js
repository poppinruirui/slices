var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var CCyberTreeMath = (function () {
    function CCyberTreeMath() {
    }
    CCyberTreeMath.Angle2Radian = function (angle) {
        return Math.PI * angle / 180;
    };
    CCyberTreeMath.Sin = function (angle) {
        return Math.sin(CCyberTreeMath.Angle2Radian(angle));
    };
    CCyberTreeMath.Cos = function (angle) {
        return Math.cos(CCyberTreeMath.Angle2Radian(angle));
    };
    // 已知位移和时间求初速度（末速度为零）
    CCyberTreeMath.GetV0 = function (s, t) {
        return (2.0 * s / t);
    };
    // 已知位移和时间求加速度（末速度为零）
    CCyberTreeMath.GetA = function (s, t) {
        return -2.0 * s / (t * t);
    };
    return CCyberTreeMath;
}()); // end class
__reflect(CCyberTreeMath.prototype, "CCyberTreeMath");
//# sourceMappingURL=CCyberTreeMath.js.map